#ifndef HEADER_H
#define HEADER_H
#include <iostream>
#include <ctime>

using namespace std;


class Encryption
{
public:
    string Message;
    string EncryptionMessage;
    char value[91];
    string codeString[91][10];

    Encryption(string message);

    void Filling();

    void Encrypt();

    void show();

};

Encryption::Encryption(string message)
{
    Message = message;
    size_t SizeEncryptionMessage = Message.size()*4;
    EncryptionMessage.resize(SizeEncryptionMessage);
}

void Encryption::Filling()
{
    int n = 0;
    for (char  i=' '; i<='z'; i++)
    {
        value[n] = i;
        n++;
    }

    for (int i=0; i<91; i++)
        for (int j=0; j<10; j++)
        {
            string BuffString;
            BuffString = "0000";
            codeString[i][j] = BuffString;

            int SumI = 0;

            while(SumI != 909)
            {
                for(int k=0; k<4; k++)
                {
                    SumI = 0;
                    int randN = rand() % 16 + 1;
                    switch (randN) {
                        case 1: codeString[i][j][k] = '0'; break;
                        case 2: codeString[i][j][k] = '1'; break;
                        case 3: codeString[i][j][k] = '2'; break;
                        case 4: codeString[i][j][k] = '3'; break;
                        case 5: codeString[i][j][k] = '4'; break;
                        case 6: codeString[i][j][k] = '5'; break;
                        case 7: codeString[i][j][k] = '6'; break;
                        case 8: codeString[i][j][k] = '7'; break;
                        case 9: codeString[i][j][k] = '8'; break;
                        case 10: codeString[i][j][k] = '9'; break;
                        case 11: codeString[i][j][k] = 'A'; break;
                        case 12: codeString[i][j][k] = 'B'; break;
                        case 13: codeString[i][j][k] = 'C'; break;
                        case 14: codeString[i][j][k] = 'D'; break;
                        case 15: codeString[i][j][k] = 'E'; break;
                        case 16: codeString[i][j][k] = 'F'; break;
                    default:
                        break;
                    }
                }
                for (int i1=0; i1<91; i1++)
                    for (int j1=0; j1<10; j1++)
                    {
                        if (codeString[i][j] != codeString[i1][j1])
                        {
                            SumI++;
                        }
                    }
            }
        }
}

void Encryption::Encrypt()
{
    for(int i = 0; i<Message.size(); i++)
    {
        int Index1 = 0;
        int Index2 = 0;
        int forI = 0;
        for (char  n=' '; n<='z'; n++)
        {
            if (Message[i] != n)
            {
                forI++;
            }
            if (Message[i] == n)
            {
                Index1 = forI;
            }
        }
        Index2 = rand() % 10;

        for(int k=0; k<4; k++)
        {
            EncryptionMessage[i*4+k] = codeString[Index1][Index2][k];
        }
    }
}



void Encryption::show()
{
    for (int i=0; i<91; i++)
    {
        cout << value[i] << ": ";
        for (int j=0; j<10; j++)
        {
            for (int k=0; k<4; k++)
            {
                cout << codeString[i][j][k];
            }
            cout << " ";
        }
        cout << endl;
    }
}



#endif // HEADER_H
